<?php get_header(); ?>

    <div id="content">
        <div class="grid clear">
            <div class="left-content">
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="entry">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="right-content">
                <nav class="sidebar-home">
                    <aside>
                        <?php dynamic_sidebar( 'home-sidebar' ); ?>
                    </aside>
                </nav>
            </div>
        </div>    
    </div>

<?php get_footer(); ?>