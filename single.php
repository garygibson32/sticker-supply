<?php get_header(); ?>

    <div id="content">
        <div class="grid clear">
            <div class="left-content">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    <div class="entry">
                        <h1><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                 
                        <?php wp_link_pages(array( 'before' => 'Pages: ', 'next_or_number' => 'number' )); ?>
                         
                        <?php the_tags( ' Tags: ', ', ', '' ); ?>
                    </div>
                    <div class="meta">
                        <?php the_time( 'F jS, Y' ); ?>
                        <br>
                        <?php edit_post_link( 'Edit this entry','','.' ); ?>
                    </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
        <div class="right-content">
            <nav class="sidebar-home">
                <aside>
                    <?php dynamic_sidebar( 'home-sidebar' ); ?>
                </aside>
            </nav>
        </div>    
    </div>

<?php get_footer(); ?>