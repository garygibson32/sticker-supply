    <footer class="footer">
        <div class="grid clear">
            <div class="navigation">
                <nav class="primary">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                </nav>
            </div>        
        </div>
    </footer>

<div id="footer-web">For <a href="http://webdesignyorkpa.com/" title="Web Design of York">Website</a> issues, contact <a href="http://webdesignyorkpa.com/" title="Web Design of York">Web Design of York</a></div>

<?php wp_footer(); ?>
</body>
</html>
