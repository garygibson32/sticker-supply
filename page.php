<?php get_header(); ?>

    <div id="content">
        <div class="grid clear">
            <div class="left-content">
                <h1><?php the_title(); ?></h1>
                <?php if ( have_posts() ):?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <h2>Not found</h2>
                <?php endif; ?>
            </div>
            <div class="right-content">
                <nav class="sidebar-home">
                    <aside>
                        <?php dynamic_sidebar( 'home-sidebar' ); ?>
                    </aside>
                </nav>
            </div>
        </div>    
    </div>

<?php get_footer(); ?>